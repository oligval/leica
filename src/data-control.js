var text = [
    data1 = "article name, authors, name of edition, publishing year, (more info?)",
    data23 = "article name, www address, (more info?) second line of text"
];


var viewModel = {};
var self = viewModel;
Item = function (name, id) {
    this.Name = name;
    this.Id = id;
};
self.deselect = true;
self.deselect2 = true;
self.deselect3 = true;
self.items = ko.observableArray([]);
self.items2 = ko.observableArray([]);
self.items3 = ko.observableArray([]);
self.selectedItems = ko.observableArray([]);
self.selectedItems2 = ko.observableArray([]);
self.selectedItems3 = ko.observableArray([]);
for (var i = 0; i < 10; i++) {
    self.items.push(new Item(text[0], i));
    self.items2.push(new Item(text[1], i));
    self.items3.push(new Item(text[1], i));
}

self.selectAllItems = ko.pureComputed({
    read: function () {
    },
    write: function () {
        self.selectedItems(self.deselect ? self.items.slice(0) : []);
        document.getElementsByClassName('footer-add')[0].classList.toggle('footer-remove');
        self.deselect = !self.deselect;
    },
    owner: self
});
self.selectAllItems2 = ko.pureComputed({
    read: function () {
    },
    write: function () {

        self.selectedItems2(self.deselect2 ? self.items2.slice(0) : []);
        document.getElementsByClassName('footer-add')[1].classList.toggle('footer-remove');
        self.deselect2 = !self.deselect2;
    },
    owner: self
});

self.selectAllItems3 = ko.pureComputed({
    read: function () {
    },
    write: function () {

        self.selectedItems3(self.deselect3 ? self.items3.slice(0) : []);
        document.getElementsByClassName('footer-add')[2].classList.toggle('footer-remove');
        self.deselect3 = !self.deselect3;
    },
    owner: self
});

self.allSelectedItems = ko.computed(function () {
    return self.selectedItems().concat(self.selectedItems2()).concat(self.selectedItems3());
}, self);

ko.applyBindings(viewModel);


function sendMail() {
    var txt = '';
    sendList = document.getElementsByClassName('selected-content')[0].innerText.split('\n');
    function appendSeparator(element) {
        return element + ('\n');
    }

    for (i = 0; i < sendList.length - 1; i++) {
        txt += (sendList[i]) + '\n';
    }

    console.log(txt);
    var link = "mailto:" + document.forms[0].email.value
            + "?cc="
            + "&subject=" + document.forms[0].Name.value
            + "&body=" + txt
        ;
    window.location.href = link;
}
