document.addEventListener('touchstart', function (event) {
    touchstartX = event.changedTouches[0].pageX;
}, false);
document.addEventListener('touchend', function (event) {
    touchendX = event.changedTouches[0].pageX;
    Handler();
});
var swiped = false;
function Handler() {
    function smth() {
        document.getElementsByClassName('indicator')[0].classList.toggle("indicator-active");
        document.getElementsByClassName('indicator')[1].classList.toggle("indicator-active");
        document.getElementsByClassName('wrap')[0].classList.toggle("animate");
        document.getElementsByClassName('form')[0].classList.toggle("animate2");
        swiped = !swiped;
    }

    if (touchendX < touchstartX && !swiped) {
        smth();
    }
    if (touchendX > touchstartX && swiped) {
        smth();
    }
}
